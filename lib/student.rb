require 'course.rb'

class Student
  attr_reader :first_name, :last_name, :courses
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(new_course)
    return if @courses.include?(new_course)
    raise "conflict" if courses.any? { |course| new_course.conflicts_with?(course) }
    @courses << new_course
    new_course.students << self
  end

  def course_load
    credits = Hash.new(0)
    @courses.each do |course|
      credits[course.department] += course.credits
    end
    credits
  end
end
